import { JSDOM, ResourceLoader } from 'jsdom';
import { parse as parseURI, resolve as resolveURI} from 'uri-js';
import fetch from 'node-fetch'
import http from 'http';
import https from 'https';
import fs from 'fs';
import path from 'path';

const baseHTMLURL = process.argv[2];

if(!validURL(baseHTMLURL))
	throw new Error('Not a valid URL');

(async () => {
	const htmlOutputDirectory = path.join(__dirname, '../public');
	const jsOutputDirectory = path.join(__dirname, '../src');

	const htmlOutputFile = 'index.html';
	const jsOutputFile = 'index.js';

	emptyDirectory(htmlOutputDirectory);
	emptyDirectory(jsOutputDirectory);

	// prevent dependencies from triggering errors when building
	let jsString = `
		/*eslint-disable no-undef*/
		/*eslint-disable no-restricted-globals*/
	`;

	const resourceLoader = new ResourceLoader({
		strictSSL: false
	});

	const dom = await JSDOM.fromURL(baseHTMLURL, { resources: resourceLoader });
	const document = dom.window.document;

	const scripts = document.querySelectorAll('script:not([data-es5-exclude])'); // get all scripts in doc, except those tagged with es5-exclude

	for(let script of scripts) {
		if(script.hasAttribute('src')) { // external script
			const scriptURL = resolveURI(baseHTMLURL, script.getAttribute('src')); // resolve relative links
			const scriptURLInfo = parseURI(scriptURL);

			// ignore certificate errors
			const fetchAgent = (scriptURLInfo.scheme === 'https')
				? new https.Agent({
					rejectUnauthorized: false
				})
				: new http.Agent();

			const scriptResponse = await fetch(scriptURL, {
				agent: fetchAgent
			});

			const scriptText = await scriptResponse.text();

			jsString = jsString.concat(scriptText);
		}
		else // inline script
			jsString = jsString.concat(script.innerHTML);

		jsString = jsString.concat('\n\n');
		script.parentNode.removeChild(script);
	}

	fs.writeFileSync(path.join(htmlOutputDirectory, htmlOutputFile), dom.serialize()); // write html file
	fs.writeFileSync(path.join(jsOutputDirectory, jsOutputFile), jsString); // write combined js file
})();

function emptyDirectory(directory) {
	const files = fs.readdirSync(directory);
	for(let file of files)
		fs.unlinkSync(path.join(directory, file));
}

function validURL(str) {
	var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
		'(\\#[-a-z\\d_]*)?$','i'); // fragment locator
	return !!pattern.test(str);
}