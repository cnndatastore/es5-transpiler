## Dependencies

- NodeJS
- Yarn (recommended) or npm

## Prepare pages for transpilation

- Add data-es5-exclude="true" to any script tags that won’t be transpiled (D3, alasql)

Example

    <script src="//cdnjs.cloudflare.com/ajax/libs/d3/5.15.0/d3.min.js" data-es5-exclude="true"></script>

...cecomes... 

    <script src="//cdnjs.cloudflare.com/ajax/libs/d3/5.15.0/d3.min.js" data-es5-exclude="true"></script>

- General HTML/Javascript hygiene: Make sure all variables are properly declared before using them, and check the console to make sure all the page resources (fonts, scripts, stylesheets) are being loaded correctly.

## How to run

    yarn install
    yarn scrape <URL>
    yarn build
    
Files are output to the build folder
